import logo from './logo.svg';
import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import { helloState } from './Redux/blank';

const App = () => {
  const { value } = useSelector(state => state.blank);
  const dispatch = useDispatch();
  return (
    <div className="App">
        <h1>
          {value}
        </h1>
        <button onClick={() => dispatch(helloState())}>Click to enable action</button>
    </div>
  );
};

export default App;

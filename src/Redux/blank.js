import { createSlice } from '@reduxjs/toolkit'

export const blankSlice = createSlice({
  name: 'list',
  initialState: {
    value: 'Hello'
  },
  reducers: {
    helloState: (state) => {
        state.value += '!'
    }
  }
})

export const { helloState } = blankSlice.actions

export default blankSlice.reducer